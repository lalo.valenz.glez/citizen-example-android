package com.example.elevenluis.myapplication;

import java.io.IOException;
import java.io.UnsupportedEncodingException;


import com.citizen.jpos.command.ESCPOS;
import com.citizen.jpos.printer.CMPPrint;
import com.citizen.jpos.printer.ESCPOSPrinter;

public class FormatosImpresion {
    private ESCPOSPrinter posPtr;
    private final char ESC = ESCPOS.ESC;
    private final char LF = ESCPOS.LF;
    private final String NORMAL = ESC + "|N";
    private final String NEGRITAS = ESC + "|bC";
    private final String SUBRAYAR = ESC + "|uC";
    private final String CENTRAR = ESC + "|cA";
    private final String ALINEAR_DERECHA = ESC + "|rA";
    private final String ALINEAR_IZQUIERDA = ESC + "|lA";
    private final String DOUBLE_WIDE = ESC + "|2C";
    // String email = "a@a.com"; //aparecera en la imagen junto con twitter y facebook
    String pagina = "www.a.com";
    String tel = "TEL (347)788 16 23";

    // 32 caracteres por linea

    public FormatosImpresion() {
        posPtr = new ESCPOSPrinter();
    }

    public void imprimirDocumento() throws UnsupportedEncodingException {
        /*try {
            //posPtr.printBitmap("sdcard/logoticket.bmp", CMPPrint.CMP_ALIGNMENT_CENTER);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        posPtr.printNormal("" + LF);
        try {

            //posPtr.printBitmap("sdcard/face.bmp", CMPPrint.CMP_ALIGNMENT_CENTER);
            // posPtr.printBitmap(Environment.getExternal+"/econ2.bmp",
            // CMPPrint.CMP_ALIGNMENT_CENTER);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }*/

        posPtr.printNormal("" + LF);

        try {
            posPtr.printNormal(CENTRAR + NEGRITAS + DOUBLE_WIDE + "Aviso de visita" + LF + LF);
            posPtr.printNormal(ALINEAR_IZQUIERDA + NEGRITAS + tel + LF);
            // posPtr.printNormal(ALINEAR_IZQUIERDA + NEGRITAS + email + LF);
            posPtr.printNormal(ALINEAR_IZQUIERDA + NEGRITAS + pagina + LF + LF);

            posPtr.printNormal("Estimado/a Sr(a)." + LF);
            //posPtr.printNormal(Variables.clienteSeleccionado.getNombre() + " " + Variables.clienteSeleccionado.getApellidoPaterno() + LF + LF);

            posPtr.printNormal("Pasamos a visitarle el dia" + LF);
            //posPtr.printNormal(StaticMethods.obtenerFecha() + LF);
            posPtr.printNormal("Le recordamos que estamos para" + LF);
            posPtr.printNormal("servirle." + LF + LF);

            posPtr.printNormal("Le atendio: " + "Francisco" + LF + LF);

            posPtr.printNormal(NEGRITAS + CENTRAR + "Gracias por su preferencia.");

            posPtr.printNormal("" + LF + LF + LF + LF + LF);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}