package com.example.elevenluis.myapplication;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.citizen.port.android.BluetoothPort;
import com.citizen.request.android.RequestHandler;

import java.io.IOException;
import java.util.Vector;

public class MainActivity extends AppCompatActivity {

    LocationManager lm;
    boolean gpsEnabled;
    private static final String TAG = "Impresion";
    // Intent request codes
    // private static final int REQUEST_CONNECT_DEVICE = 1;
    private static final int REQUEST_ENABLE_BT = 2;

    ArrayAdapter<String> adapter;
    private BluetoothAdapter mBluetoothAdapter;
    private Vector<BluetoothDevice> remoteDevices;
    private Thread hThread;
    private static boolean connected;
    private Context context;
    // UI
    private Button connectButton;
    private Button imprimirButton;
    private ListView list;
    // BT
    private BluetoothPort bp;

    BluetoothDevice remoteDevice;//iniciar en el create

    public void operacionImprimir(int index) {

        try {
            switch (index) {

                case 1:
                        try {
                            new FormatosImpresion().imprimirDocumento();
                            //Alertas.mostrarToast(ImprimirActivity.this, "Documento impreso");

                        } catch (Exception e) {
                            Log.d("Insertar LocalDB", e + "");
                            //StaticMethods.generarErrorLog(e);
                            //Alertas.showError("No se logr� imprimir o guardar informaci�n" + e.toString(), ImprimirActivity.this);
                        }
                    break;

            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
    }

    /**
     * Set up Bluetooth.
     */
    private void bluetoothSetup() {
        // Initialize
        clearBtDevData();
        bp = BluetoothPort.getInstance();
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            // Device does not support Bluetooth
            return;
        }
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);

            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }
    }

	/*
	 * @Override protected void onResume() { // TODO Auto-generated method stub operacionImprimir(); super.onResume(); }
	 */

    private void clearBtDevData() {
        remoteDevices = new Vector<BluetoothDevice>();
    }

    // For the Desire Bluetooth close() bug.
    private void connectInit() {
        if (!mBluetoothAdapter.isEnabled()) {
            mBluetoothAdapter.enable();
            try {
                Thread.sleep(3600);
            } catch (Exception e) {
                Log.e(TAG, e.getMessage(), e);
            }
        }
    }

    // Bluetooth Connection method.
    private void btConn(final BluetoothDevice btDev) throws IOException {
        new connTask().execute(btDev);
    }

    private void btDisconn() {
        try {
            bp.disconnect();
            Thread.sleep(1200);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage(), e);
        }

        if ((hThread != null) && (hThread.isAlive()))
            hThread.interrupt();
        connected = false;
        // UI
        connectButton.setText("Desconectar");
        connectButton.setEnabled(false);
        list.setEnabled(true);
        imprimirButton.setEnabled(false);
        Toast.makeText(context, "", Toast.LENGTH_SHORT).show();
        Toast.makeText(context, "Bluetooth Desconectado", Toast.LENGTH_SHORT).show();
    }

    /**
     * Bluetooth connection status.
     *
     * @return connected - boolean
     */
    public static boolean isConnected() {
        return connected;
    }

    @Override
    protected void onDestroy() { // TODO Auto-generated method stub

        super.onDestroy();

        try {
            bp.disconnect();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            // Log.e(TAG, e.getMessage(), e);
        }

        if ((hThread != null) && (hThread.isAlive())) {
            hThread.interrupt();
            hThread = null;
        }
    }
    //al parecer este metodo genera error en las impresoras chinas

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Impresora impresoraPredeterminada = OperacionesLocalDB.obtenerImpresora(ImprimirActivity.this);

        //iniciar botones
        context = this;
        connectButton = (Button) findViewById(R.id.btnConectarImpresora);
        imprimirButton = (Button) findViewById(R.id.btnImprimir);
        try{
            // revisar que el bluetooth este encendido
            bluetoothSetup();

            //agregar impresora
            list = (ListView) findViewById(R.id.listViewImpresoras);
            adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);
            list.setAdapter(adapter);
            String key;
            remoteDevice = mBluetoothAdapter.getRemoteDevice("00:13:7B:5A:EC:6E");
            if (remoteDevice != null) {
                key = remoteDevice.getName() + "\n[" + remoteDevice.getAddress() + "]";

                remoteDevices.add(remoteDevice);
                adapter.add(key);
            }

            // Connect -
            list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                    // TODO Auto-generated method stub
                    BluetoothDevice btDev = remoteDevices.elementAt(arg2);
                    connectInit();
                    try {
                        if (!connected)
                            btConn(btDev);
                        else
                            return;
                    } catch (IOException e) {
                        //Alertas.showError(e.getMessage(), context);
                        return;
                    }
                }
            });

            //si el dispositivo no esta conectado, entonces conectar al inicio
            conectarDesconectarImpresora();


            //al final checar los botones de impresion
            operacionImprimir();
            // imprimirButton.setEnabled(true);//para pruebas descomentar
        }
        catch(Exception e){
            e.printStackTrace();
            //Alertas.mostrarToast(this, "Impresora no detectada");
        }
    }

    private void conectarDesconectarImpresora(){
        // Connect routine.
        if (!connected) {
            connectInit();
            try {
                btConn(remoteDevice);
            } catch (IllegalArgumentException e) {
                // Bluetooth Address Format [OO:OO:OO:OO:OO:OO]
                Log.e(TAG, e.getMessage(), e);
                //Alertas.showError(e.getMessage(), context);
                return;
            } catch (Exception e) {
                Log.e(TAG, e.getMessage(), e);
                //Alertas.showError(e.getMessage(), context);
                return;
            }
        }
        // Disconnect routine.
        else {
            // Always run.
            btDisconn();
        }
    }

    public void operacionImprimir() {
        //this.setTitle(Variables.operacionImprimir.getTitulo());

        if (!connected) {

            connectButton.setText("Conectar");
            connectButton.setEnabled(true);
            // Imprimir
            imprimirButton.setEnabled(false);
            imprimirButton.setOnClickListener(new View.OnClickListener() {

                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    operacionImprimir(1);
                }
            });

            // Connect, Disconnect -- Button
            connectButton.setOnClickListener(new View.OnClickListener() {

                public void onClick(View v) {
                    // Connect routine.
                    conectarDesconectarImpresora();
                }
            });
        }// if
        else {
            connected = true;

            connectButton.setText("Desconectar");
            connectButton.setEnabled(true);

            // Imprimir
            imprimirButton.setEnabled(true);
            // Button btnImprimir=(Button)findViewById(R.id.btnImprimir);
            imprimirButton.setOnClickListener(new View.OnClickListener() {

                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    operacionImprimir(1);
                }
            });

            // Connect, Disconnect -- Button
            connectButton.setOnClickListener(new View.OnClickListener() {

                public void onClick(View v) {
                    // Connect routine.
                    conectarDesconectarImpresora();
                }
            });
        }// else
    }



    /** ======================================================================= */
    public class connTask extends AsyncTask<BluetoothDevice, Void, Integer> {
        private final ProgressDialog dialog = new ProgressDialog(MainActivity.this);

        @Override
        protected void onPreExecute() {
            dialog.setTitle("Bluetooth");
            dialog.setMessage("Conectando");
            dialog.show();
            super.onPreExecute();
        }

        @Override
        protected Integer doInBackground(BluetoothDevice... params) {
            Integer retVal = null;
            try {
                bp.connect(params[0]);
                retVal = new Integer(0);
            } catch (IOException e) {
                retVal = new Integer(-1);
            }
            return retVal;
        }

        @Override
        protected void onPostExecute(Integer result) {
            if (result.intValue() == 0) {
                RequestHandler rh = new RequestHandler();
                hThread = new Thread(rh);
                hThread.start();
                connected = true;
                // UI
                connectButton.setText("Desconectar");
                connectButton.setEnabled(true);
                imprimirButton.setEnabled(true);
                list.setEnabled(false);
            }
            if (dialog.isShowing()) {
                String cmsg;
                dialog.dismiss();
                if (connected) {
                    cmsg = "Bluetooth Conectado";
                    Toast toast = Toast.makeText(context, cmsg, Toast.LENGTH_SHORT);
                    toast.show();
                } else {
                    cmsg = "Bluetooth No Conectado.";
                    //Alertas.showError(cmsg, context);
                }
            }
            super.onPostExecute(result);
        }
    }
}
